# regular file, dir, link

#! /bin/bash
# Sergi Font ASIX M01-ISO
# Febrer 2022
# ------------------------------
#1) si num args no es correcte plegar

ERR_NARGS=1
ERR_NO_EXISTS=2

if [ $# -ne 1 ];
then
	echo "Error: numero d'arguments incorrecte"
	echo "Usage: $0 arxiu"
	exit $ERR_NARGS

fi

#2) xixa

fit=$1

if [ ! -e $fit ];
then
	echo "$fit no existeix"
	exit $ERR_NOEXIST

elif [ -f $fit ];
then
	echo "$fit es un regular file"

elif [ -h $fit ];
then
	echo "$fit es un symbolic link"

elif [ -d $fit ];
then
	echo "$fit es un directori"
else
	echo "$fit es un altre tipus de fitxer"
fi
exit 0
