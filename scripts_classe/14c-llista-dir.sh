#! /bin/bash
# @edt ASIX M01-ASO Curs 2021-2022
# Febrer 2022
# $ llistar-dir.sh dir
# ampliat indicant per cada element si és link, dir, 
# regular o altra cosa
# -------------------------------------
ERR_NARGS=1
ERR_NODIR=2
# 1) validar arguments
if [ $# -ne 1 ]
then
  echo "Error: número args no vàlid"
  echo "usage: $0 dir"
  exit $ERR_NARGS
fi
dir=$1

# 2) validar arg és un dir
if ! [ -d $dir ]
then
  echo "Error: $dir no és un directori"
  echo "usage: $0 dir"
  exit $ERR_NODIR
fi
# 3) xixa
llista_dir=$(ls $dir)
for nom in $llista_dir
do
  if [ -h "$dir/$nom" ]; then
    echo "$nom és un link"
  elif [ -d "$dir/$nom"  ]; then
    echo "$nom és un dir"
  elif [ -f "$dir/$nom" ]; then
    echo "$nom és un regular"      	  
  else
    echo "$nom és una altra cosa"
  fi  
done	
exit 0


