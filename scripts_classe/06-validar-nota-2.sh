#! /bin/bash
# Sergi Font ASIX M01-ISO
# Febrer 2022
# validar nota: suspès, aprovat, notable o excel·lent
# ------------------------------
#1) si num args no es correcte plegar

ERR_NARGS=1
ERR_NOTA=2

if [ $# -ne 1 ]
then
	echo "Error: numero d'arguments incorrecte"
	echo "Usage: $0 nota"
	exit $ERR_NARGS
fi

#2) validar rang nota

if [ $1 -lt 0 -o $1 -gt 10 ]
then
	echo "Error: nota $1 no valida"
	echo "la nota no pren valors de 0-10"
	echo "Usage: $0 nota entre [0-10]"
	exit $ERR_NOTA
fi

#3) Xixa

nota=$1

if [ $1 -lt 5 ]
then
	echo "$nota es un suspes"
elif
	[ $nota -lt 7 ];
then
	echo "$nota es un aprovat"
elif
	[ $nota -lt 9 ];
then
	echo "$nota es un notable"
else
	echo "$nota es un Excel·lent"
fi
exit 0
