#! /bin/bash
# Febrer 2022 
# Curs: ASIX M01
# Autor: Sergi Font
# Exemples bucle for
# -------------------------

# 8) llistar els logins enumerats i ordenats

llista_login=$(cut -d: -f1 /etc/passwd | sort)
num=1

for element in $llista_login
do
	echo "$num: $element"
	((num++))
done
exit 0


# 7) llistar enumerat els noms dels files
# del directori actiu

llista_noms=$(ls)
num=1

for element in $llista_noms
do
	echo "$num: $element"
	((num++))
done
exit 0



# 6) llistar els arguments enumerats

num=0

for arg in $*
do
	echo "$num: $arg"
	num=$((num+1))
done
exit 0


# 5) iterar per la llista d'arguments
# $@ expandeix els words encara que
# estiguin encapsulats

for arg in "$@"
do
	echo $arg
done
exit 0


# 4) iterar per la llista d'arguments

for arg in $*
do
	echo $arg
done
exit 0


# 3) iterar pel valor d'una variable

llistat=$(ls)

for nom in $llistat
do
	echo $nom
done
exit 0


# 2) iterar per un conjunt d'elements

for nom in "pere pau marta anna"
do
	echo "$nom"
done
exit 0


# 1) iterar per un conjunt d'elements

for nom in "pere" "pau" "marta" "anna"
do
	echo "$nom"
done
exit 0
