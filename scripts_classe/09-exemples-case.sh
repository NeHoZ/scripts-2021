#! /bin/bash
# Febrer 2022 
# Curs: ASIX M01
# Autor: Sergi Font
# Exemples Case
# -------------------------

# dl dt dc dj dv laborabl
# ds festiu

case $1 in
	"dilluns"|"dimarts"|"dimecres"|"dijous"|"divendres")
		echo "$1 és un dia laboral";;
	"dissabte"|"diumenge")
		echo "$1 és un dia festiu";;
	*)
		echo "$1 és una altre cosa"

esac
exit 0






case $1 in
	[aeiou])
		echo "$1 és una vocal";;
	[bcdfghjklmnpqrstvwxyz])
		echo "$1 és una consonant";;
	*)
		echo "$1 és una altre cosa"
esac
exit 0



case $1 in
	"pere"|"pau"|"joan")
		echo "és un nen"
		;;
	"marta"|"anna"|"julia")
		echo "és una nena"
		;;
	*)
		echo "és indefinit"
		;;
esac
exit 0
