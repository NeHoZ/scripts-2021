#! /bin/bash
# Sergi Font ASIX M01-ISO
# Febrer 2022
# validar nota: suspès o aprovat
# ------------------------------
#1) si num args no es correcte plegar

if [ $# -ne 1 ]
then
	echo "Error: numero d'arguments incorrecte"
	echo "Usage: $0 edat"
	exit 1
fi

#2) validar rang nota

nota=$1

if [ $1 -lt 0 -o -gt 10 ]
then
	echo "Error: rang d'arguments incorrecte"
	echo "Usage: $0 nota"
	exit 2
fi

#3) Xixa

nota=$1

if [ $nota -lt	5 ]
then
	echo "$nota es un suspes"
else
	echo "$nota es un aprovat"

fi
exit 0

