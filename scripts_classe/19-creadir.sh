#! /bin/bash
# Sergi Font ASIX-M01,
# Febrer 2022
# crear n directoris
# almenys 1 arg
# mkdir no genera cap sortida
# 0 tots dir creats ok
# 1 error numero args
# 2 si algun dir no s'ha pogut crear
#--------------------------------------------

ERR_NARGS=1
ERR_MKDIR=2
stat=0

if [ $# -lt 1 ];
then
	echo "Error: numero d'arguments incorrecte"
	echo "Usage: $0 dir1 dir2 dir3..."
	exit $ERR_NARGS
fi

for nom in $*
do

mkdir $nom &> /dev/null

if [ $? -ne 0 ];
then
	echo "Error: el directori $nom no s'ha pogut crear" >&2
	stat=$ERR_MKDIR
fi
done
exit $stat
