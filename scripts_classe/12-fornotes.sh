#! /bin/bash
# Febrer 2022 
# Curs: ASIX M01
# Autor: Sergi Font
# Exemples bucle for
# -------------------------

# 1 argument o mes
# per cada nota diu si suspes. aprovat, not, exce...
# nota [0-10]
# validar num args

#1) si num args no es correcte plegar

ERR_NARGS=1
ERR_NOTA=2

if ! [ $# -ge 1 ]
then
	echo "Error: numero d'arguments incorrecte"
	echo "Usage: $0 nota"
	exit $ERR_NARGS
fi

#2) iterar per cada nota

nota=$1

llista_notes=$*

for nota in $llista_notes
do

if [ $nota -lt 0 -o $nota -gt 10 ]
then
	echo "Error: nota $nota no vàlida [0-10]" >> /dev/stderr

elif [ $nota -lt 5 ]
then
	echo "$nota es un suspes"
elif
	[ $nota -lt 7 ];
then
	echo "$nota es un aprovat"
elif
	[ $nota -lt 9 ];
then
	echo "$nota es un notable"
else
	echo "$nota es un Excel·lent"
fi
done
exit 0
