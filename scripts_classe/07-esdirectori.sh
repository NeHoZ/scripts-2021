#! /bin/bash
# Sergi Font ASIX M01-ISO
# Febrer 2022
# ------------------------------
#1) si num args no es correcte plegar

ERR_NARGS=1
ERR_NO_DIRECTORI=2

if [ $# -ne 1 ];
then
	echo "Error: numero d'arguments incorrecte"
	echo "Usage: $0 arxiu"
	exit $ERR_NARGS

fi

#2) sino es un directori error

if [ ! -d $1 ];
then
	echo "Error: $1 no es un directori"
	echo "Usage: $0 nota"
	exit $ERR_NO_DIRECTORI
fi

#3) si es un dir llistar-lo

dir=$1
ls $dir
exit 0
