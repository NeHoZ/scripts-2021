#! /bin/bash
# Febrer 2022 
# Curs: ASIX M01
# Autor: Sergi Font
# Excercicis scripts basics
# -------------------------

# 10) Fer un programa que rep com a argument un número indicatiu 
# del número màxim de línies a mostrar. El programa processa stdin 
# línia a línia i mostra numerades un màxim de num línies.

# 0) Definir constants per errors

ERR_NARGS=1
ERR_NODIR=2

# 1) validar arguments

if [ $# -lt 1 -o $# -gt 2 ]
then
  	echo "Error: número args no vàlid"
  	echo "usage: $0 numero [arxiu]"
  	exit $ERR_NARGS
fi

# 2) Xixa

MAX=$1

num=1

while read -r line
do	
	echo "$num: $line "
	((num++))

if [ $num -gt $MAX ]
then
	exit 0

fi
done
exit 0
"""
