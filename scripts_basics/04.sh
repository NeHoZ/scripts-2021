#! /bin/bash
# Febrer 2022 
# Curs: ASIX M01
# Autor: Sergi Font
# Excercicis scripts basics
# -------------------------


# 0) Defeinir constants per errors

ERR_NARGS=1

# 1) Validar Arguments

if [ $# -lt 1 ]
then
       	echo "Error, numero d'arguments no valid"
	echo "Usage: $0 mes"
  	exit $ERR_NARGS
fi

# 2) Xixa

mes=$1

llista_mesos=$*

for mes in $llista_mesos
do
   if ! [ $mes -ge 1 -a $mes -le 12 ]
   then
       	echo "Error, mes $mes no vàlid" 1>&2
       	echo "Mes pren valors del [1-12]"
else
	case "$mes" in
  	"2") 
	dies=28;;
 	"4"|"6"|"9"|"11") 
	dies=30;;
	*)
	dies=31;;
esac
        echo "El més: $mes, té $dies dies"
fi
done
exit 0

