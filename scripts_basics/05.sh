#! /bin/bash
# Febrer 2022 
# Curs: ASIX M01
# Autor: Sergi Font
# Excercicis scripts basics
# -------------------------

# Excercici 05
# Mostrar línia a línia l’entrada estàndard, 
# retallant només els primers 50 caràcters.

# 1) Xixa

while read -r line
do
	echo "$line" | cut -c1-50
done
exit 0
