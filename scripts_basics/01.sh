#! /bin/bash
# Febrer 2022 
# Curs: ASIX M01
# Autor: Sergi Font
# Excercicis scripts basics
# -------------------------

# Excercici 01
# Mostrar l’entrada estàndard numerant línia a línia

num=1

while read -r line
do
	echo "$num: $line " | tr 'a-z' 'A-Z'
	((num++))
done
exit 0
