#! /bin/bash
# Febrer 2022 
# Curs: ASIX M01
# Autor: Sergi Font
# Excercicis scripts basics
# -------------------------
# 9) Fer un programa que rep per stdin noms d’usuari (un per línia), 
# si existeixen en el sistema (en el fitxer /etc/passwd) 
# mostra el nom per stdout. Si no existeix el mostra per stderr.

# 1) Xixa

while read -r line
do

grep -q "^$line:" /etc/passwd

if [ $? -ne 0 ];
then
	echo "Error, El usuari $line no existeix" 1>&2
else
	echo "$line"

fi
done
exit 0



