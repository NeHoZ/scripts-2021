#! /bin/bash
# Febrer 2022 
# Curs: ASIX M01
# Autor: Sergi Font
# Excercicis scripts basics
# -------------------------
# 8. Fer un programa que rep com a argument noms d’usuari, 
# si existeixen en el sistema (en el fitxer /etc/passwd) 
# mostra el nom per stdout. Si no existeix el mostra per stderr.

# 0) Defeinir constants per errors

ERR_NARGS=1
ERR_NOEXIST=2

# 1) Validar Arguments

if [ $# -lt 1 ]
then
       	echo "Error, numero d'arguments no valid"
	echo "Usage: $0 nom d'usuari"
  	exit $ERR_NARGS
fi

# 2) Xixa

for usuari in $*
do

grep -q "^$usuari:" /etc/passwd

if [ $? -ne 0 ];
then
	echo "Error, El usuari $usuari no existeix" 1>&2
else
	echo "$usuari"

fi
done
exit 0



