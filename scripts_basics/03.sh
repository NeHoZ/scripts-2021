#! /bin/bash
# Febrer 2022 
# Curs: ASIX M01
# Autor: Sergi Font
# Excercicis scripts basics
# -------------------------

# Excercici 03
# Fer un comptador des de zero fins al valor indicat per l’argument rebut.

limit=$1

comptador=0

while [ $comptador -lt $limit ]
do
	((comptador++))
	echo "$comptador"
done
exit 0
