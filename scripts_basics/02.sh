#! /bin/bash
# Febrer 2022 
# Curs: ASIX M01
# Autor: Sergi Font
# Excercicis scripts basics
# -------------------------

# Excercici 02
# 1) Mostar els arguments rebuts línia a línia, tot numerànt-los.

num=1

llista_args=$*

for arg in $llista_args
do
	echo "$num: $arg "
	((num++))
done
exit 0
