#! /bin/bash
# Febrer 2022 
# Curs: ASIX M01
# Autor: Sergi Font
# Excercicis scripts basics
# -------------------------

# Excercici 6

# Fer un programa que rep com a arguments noms de dies de la setmana 
# i mostra  quants dies eren laborables i quants festius. 
# Si l’argument no és un dia de la setmana genera un error per stderr.
# Exemple: $ prog dilluns divendres dilluns dimarts kakota dissabte sunday

# 0) Defeinir constants per errors

ERR_NARGS=1

# 1) Validar Arguments

if [ $# -lt 1 ]
then
       	echo "Error, numero d'arguments no valid"
	echo "Usage: $0 dia de la setmana"
  	exit $ERR_NARGS
fi

# 2) Xixa

dia=$1

llista_dies=$*

for dia in $llista_dies
do
	case $dia in
	"dilluns"|"dimarts"|"dimecres"|"dijous"|"divendres")
		echo "$dia és un dia laboral";;
	"dissabte"|"diumenge")
		echo "$dia és un dia festiu";;
	*)
	        echo "Dia $dia és una altre cosa" 1>&2
	        echo "Dia $dia ha de ser un dia de la setmana: dilluns, dimarts..." 1>&2

esac
done
exit 0
