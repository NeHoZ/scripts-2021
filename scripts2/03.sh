#! /bin/bash
# Febrer 2022 
# Curs: ASIX M01
# Autor: Sergi Font
# Excercicis scripts 2
# -------------------------

# Excerci 03)
# Processar arguments que són matricules: 

# a) llistar les vàlides, del tipus: 9999-AAA. 

# b) stdout les que són vàlides, per stderr les no vàlides. 

# Retorna de status el número d’errors (de no vàlides)
#------------------------------------------------------

# 0) Definir constants per errors

ERR_NARGS=1

# 1) validar arguments

if [ $# -lt 1 ]
then
        echo "Error: número args no vàlid"
        echo "usage: $0 paraula"
        exit $ERR_NARGS
fi

# 2) Xixa

num=0

for dni in $*
do

comprovar_dni=$(echo "$dni" | grep -E "^[0-9]{4}-[A-Z]{3}$")

if [ $? -ne 0 ]
then
	echo "$dni" > /dev/stderr
	num=$((num+1))
else
	echo "$dni"
fi
done

echo "status errors: $num"

exit 0
