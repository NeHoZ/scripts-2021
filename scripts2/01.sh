#! /bin/bash
# Febrer 2022 
# Curs: ASIX M01
# Autor: Sergi Font
# Excercicis scripts 2
# -------------------------

# Excercici 01
# Processar els arguments i mostrar per stdout només els de 4 o més caràcters.

# 1) Xixa

for char in $*
do

count_chars=$(echo -n "$char" | wc -m)

if [ $count_chars -ge 4 ]
then
	echo "$char"
fi
done
exit 0
