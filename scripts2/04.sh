#! /bin/bash
# Febrer 2022 
# Curs: ASIX M01
# Autor: Sergi Font
# Excercicis scripts 2
# -------------------------

# Excerci 04)
# Processar stdin mostrant per stdout les línies numerades i en majúscules..

# 1) Xixa

num=1

while read -r line
do
  echo "$num: $line " | tr 'a-z' 'A-Z'
  ((num++))
done
exit 0

