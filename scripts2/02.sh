#! /bin/bash
# Febrer 2022 
# Curs: ASIX M01
# Autor: Sergi Font
# Excercicis scripts 2
# -------------------------

# Excerci 02)
# Processar els arguments i comptar quantes n'hi ha
# de 3 o més caràcters.

# 0) Definir constants per errors

ERR_NARGS=1

# 1) validar arguments

if [ $# -lt 1 ]
then
        echo "Error: número args no vàlid"
        echo "usage: $0 paraula"
        exit $ERR_NARGS
fi

# 2) Xixa

num=0

for char in $*
do

count_chars=$(echo -n "$char" | wc -m)
	
if [ $count_chars -ge 3 ]
then
	((num++))
fi	
done

echo "$num"

exit 0
