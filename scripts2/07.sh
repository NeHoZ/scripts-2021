#! /bin/bash
# Febrer 2022 
# Curs: ASIX M01
# Autor: Sergi Font
# Excercicis scripts 2
# -------------------------

# Excerci 07)

# Programa: prog -f|-d arg1 arg2 arg3 arg4

# a) Valida que els quatre arguments rebuts són tots del 
# tipus que indica el flag. És a dir, si es crida amb 
# -f valida que tots quatre són file. Si es crida amb 
# -d valida que tots quatre són directoris.
# Retorna 0 ok, 1 error nº args, 2 hi ha elements errònis. 
# Exemple: prog -f carta.txt a.txt /tmp fi.txt → retorna status 2. 

# b)

ERR_NARGS=1
ERR_ELEMENTS=2

#1) Validar existeix un arg

if [ $# -lt 1 ]
then
	echo "Error, numero d'arguments no valid"
	echo "Usage: $0 mes"
	exit $ERR_NARGS
fi

#2 validar si help

if [ "$1" = "-h" -o "$1" = "--help" ]
then
	echo "Escola del treball"
  	echo "@edt ASIX-M01"
  	echo "Usage: $0 mes"
  	exit 0
fi

#3) xixa: determinar numero dies del més

case "$1" in
  "-f")

shift

for arg in $*
do

if ! [ -f "$arg" ]
then
	echo "Error: hi ha elements erronis" 1>&2
fi
done
echo "tot ok";;
  
  "-d")
;;

*)

;;

esac
exit 0
