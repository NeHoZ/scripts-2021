#! /bin/bash
# Febrer 2022 
# Curs: ASIX M01
# Autor: Sergi Font
# Excercicis scripts 2
# -------------------------

# Excerci 06)
# Processar per stdin linies d’entrada tipus “Tom Snyder” 
# i mostrar per stdout la línia en format → T. Snyder.  
# --------------------------------------------------------

# 01) Xixa

while read -r line
do
 
first_letter=$(echo "$line" | cut -c1)

surname=$(echo "$line" | cut -d' ' -f2)

echo "$first_letter. $surname." 

done
exit 0

