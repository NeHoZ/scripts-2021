#! /bin/bash
# Febrer 2022 
# Curs: ASIX M01
# Autor: Sergi Font
# Excercicis scripts 2
# -------------------------

# Excercici 08)

# Programa: prog file…

# a) Validar existeix almenys un file. Per a cada file comprimir-lo. 
# Generar per stdout el nom del file comprimit si s’ha comprimit correctament,# o un missatge d’error per stderror si no s’ha pogut comprimir. 
# En finalitzar es mostra per stdout quants files ha comprimit. 
# Retorna status 0 ok, 1 error nº args, 2 si algun error en comprimir.

# b) Ampliar amb el cas: prog -h|--help.

# 0) Control errors

OK=0
ERR_NARGS=1
ERR_NO_COMPRIMIR=2

#1) Validar existeix un arg

if [ $# -lt 1 ]
then
    echo "Error, numero d'arguments no valid"
    echo "Usage: $0 mes"
    exit $ERR_NARGS
fi

#2 validar si help

if [ "$1" = "-h" -o "$1" = "--help" ]
then
  echo "Escola del treball"
  echo "@edt ASIX-M01"
  echo "Usage: $0 mes"
  exit $OK
fi

# 3) Xixa

num=0

for arg in $*
do
	gzip $arg 2> /dev/null

if [ $? = 0 ]
then
	echo "$arg.gz"
	num=$((num+1))
else
	echo "$arg No s'ha pogut comprimir" 1>&2
fi
done

echo "$num files comprimits"
exit $OK
