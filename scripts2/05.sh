#! /bin/bash
# Febrer 2022 
# Curs: ASIX M01
# Autor: Sergi Font
# Excercicis scripts 2
# -------------------------

# Excerci 05)
# Processar stdin mostrant per stdout les línies de menys de 50 caràcters.

# 1) Xixa

while read -r line
do
 
count_chars=$(echo "$line" | wc -c)

if [ $count_chars -lt 50 ]
then
	echo "$line"
fi
done
exit 0

